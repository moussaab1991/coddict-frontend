const http = axios.create({
  baseURL: "http://localhost:8000/api/auth",
  timeout: 1000,
  headers: { "Content-Type": "application/json" }
});