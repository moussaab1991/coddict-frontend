import LocalStorage from "./LocalStorage";
import Requests from "./Requests";

class Auth {
  constructor() {
    //this.authenticated = false;
    this.authenticated = this.isAuthenticated();
    this.user = {};
  }

  login(userData, cb) {
    //this.authenticated = true;

    Requests.post("/login", userData).then(response => {
      let token = response.data.access_token;
      LocalStorage.setStorage("token", token, () => {
        this.authenticated = this.isAuthenticated();
        cb();
      });
    });
  }

  logout(cb) {
    //this.authenticated = false;
    Requests.get("/logout").then(response => {
      LocalStorage.removeStorage("token", () => {
        this.authenticated = this.isAuthenticated();
        cb();
      });
    });
  }

  isAuthenticated() {
    // return this.authenticated;
    return LocalStorage.checkStorage("token");
  }

  isAllowed(user, rights) {
    rights.some(right => user.rights.includes(right));
  }

  hasRole(user, roles) {
    roles.some(role => user.roles.includes(role));
  }
}
export default new Auth();
// export const isAuthenticated = user => !!user;

// export const isAllowed = (user, rights) =>
//   rights.some(right => user.rights.includes(right));

// export const hasRole = (user, roles) =>
//   roles.some(role => user.roles.includes(role));
