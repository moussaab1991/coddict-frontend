import React from "react";
import { Route, Redirect } from "react-router-dom";
import auth from "../../config/auth";
import Header from "../NavBar/Header";

export const ProtectedRoute = ({ component: Component, ...rest }) => {
  return (
    <div>
      {" "}
      <Header />
      <Route
        {...rest}
        render={props => {
          // alert(auth.isAuthenticated());
          if (auth.isAuthenticated()) {
            console.log("props in ProtectedRouteIS", props);
            return <Component {...props} />;
          } else {
            return (
              <Redirect
                to={{ pathname: "/", state: { from: props.location } }}
              />
            );
          }
        }}
      />
    </div>
  );
};
