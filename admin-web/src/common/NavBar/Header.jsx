import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";

import auth from "../../config/auth";
import Requests from "../../config/Requests";

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = { isAdmin: false };
  }
  componentWillMount() {
    // you shoud add to class roles.js for design patern
    Requests.get("/user").then(response => {
      let isAdminRes = this.isAdmin(response.data.type);
      this.setState({ isAdmin: isAdminRes });
    });
  }
  isAdmin = userInfo => {
    if (userInfo == "admin") {
      return true;
    } else {
      return false;
    }
  };

  showRegister = () => {
    return this.state.isAdmin ? (
      <Link className="nav-link" to="/register">
        AddUser
      </Link>
    ) : null;
  };
  logOut = () => {
    auth.logout(() => {
      // this.props.history.goBack("/");

      this.props.history.push("/");
    });
  };

  render() {
    return (
      <div>
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <Link className="navbar-brand" to="/editable">
            All
          </Link>
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon" />
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item">
                <Link className="nav-link" to="/add">
                  AddShortLink
                </Link>
              </li>
              <li className="nav-item">{this.showRegister()}</li>
              <li className="nav-item">
                {/* <Link className="nav-link" to="/reset/password">
                  Reset Password
                </Link> */}
              </li>
              <li className="nav-item">
                <button
                  className="btn btn-outline-dark my-2 my-sm-0"
                  onClick={this.logOut}
                >
                  Logout
                </button>
              </li>
              <li className="nav-item">
                {/* <Link
                  className="nav-link disabled"
                  to="/"
                  tabIndex={-1}
                  aria-disabled="true"
                >
                  Disabled
                </Link> */}
              </li>
            </ul>
            {/* <form className="form-inline my-2 my-lg-0">
              <input
                className="form-control mr-sm-2"
                type="search"
                placeholder="Search"
                aria-label="Search"
              />
              <button
                className="btn btn-outline-success my-2 my-sm-0"
                type="submit"
              >
                Search
              </button>
            </form> */}
          </div>
        </nav>
      </div>
    );
  }
}

export default withRouter(Header);
