import React, { Component } from "react";

import { BrowserRouter, Route, Switch } from "react-router-dom";
import { ProtectedRoute } from "./common/protectedRoute/ProtectedRoute";
import { ProtectedRouteB } from "./common/protectedRoute/ProtectedRouteB";
import LoginForm from "./pages/LogInPage/LoginForm";
// we added this rout to redirect to login and doesnot apear 404
import Add from "./pages/Add/Add";
import Register from "./pages/Register/Register";
import All from "./pages/All/All";
import ResetPassword from "./pages/ResetPassword/ResetPassword";

import "./App.css";

class App extends Component {
  state = {
    loggedIn: false
  };

  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={LoginForm} />
          <ProtectedRoute exact path="/editable" component={All} />

          <ProtectedRoute exact path="/add" component={Add} />
          <ProtectedRouteB exact path="/register" component={Register} />
          <ProtectedRoute
            exact
            path="/reset/password"
            component={ResetPassword}
          />

          <Route path="*" component={() => "404 NoT Found"} />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
