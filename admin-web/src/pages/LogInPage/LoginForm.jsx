import React, { Component } from "react";
import auth from "../../config/auth";
import "bootstrap/dist/css/bootstrap.css";
import "./LoginForm.css";

class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.checkIslogIn();
  }

  checkIslogIn() {
    if (auth.isAuthenticated()) {
      this.props.history.push("/editable");
    }
  }
  handleSubmit = e => {
    let email = e.target.email.value;
    let password = e.target.password.value;
    let formData = { email: email, password: password, remember_me: true };

    e.preventDefault();
    auth.login(formData, () => {
      this.props.history.push("/editable");
    });

    if (email && password) {
      e.target.email.value = "";
      e.target.password.value = "";
    }
  };

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="login-wrap">
            <form className="form-horizontal" onSubmit={this.handleSubmit}>
              <div className="form-group">
                <label htmlFor="inputEmail3" className="col-sm-3 control-label">
                  UserName
                </label>
                <div className="col-sm-9">
                  <input
                    name="email"
                    type="email"
                    className="form-control"
                    id="inputUser"
                    placeholder="UserName"
                    required
                  />
                </div>
              </div>
              <div className="form-group">
                <label
                  htmlFor="inputPassword3"
                  className="col-sm-3 control-label"
                >
                  Password
                </label>
                <div className="col-sm-9">
                  <input
                    name="password"
                    type="password"
                    className="form-control"
                    id="inputPsw"
                    placeholder="Password"
                    required
                  />
                </div>
              </div>
              <div className="form-group">
                <div className="col-sm-offset-3 col-sm-9">
                  <div className="checkbox">
                    <label>
                      <input type="checkbox" /> Remember me
                    </label>
                  </div>
                </div>
              </div>
              <div className="form-group last">
                <div className="col-sm-offset-3 col-sm-9">
                  <button type="submit" className="btn btn-success btn-sm">
                    Sign in
                  </button>
                  <button type="reset" className="btn btn-default btn-sm">
                    Reset
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default LoginForm;
