import React, { Component } from "react";
import "./Edite.css";
import Requests from "../../config/Requests";
import { checkUrl } from "../../config/helper";
class Edite extends Component {
  constructor(props) {
    super(props);
    this.state = {
      url_id: this.props.url.id,
      url_key: this.props.url.url_key,
      url: this.props.url.url
    };
  }

  handleChange = e => {
    e.preventDefault();
    //Handling Multiple Inputs ;
    const target = e.target;
    const name = target.name;
    let value = target.value;

    this.setState({
      [name]: value
    });
    console.log(this.state);
  };

  onFormSubmit = e => {
    e.preventDefault();
    let urlWeb = checkUrl(this.state.url);
    if (urlWeb) {
      const formData = {
        id: this.state.url_id,
        url_key: this.state.url_key,
        url: this.state.url
      };
      this.props.onBtnClick("update", formData);
    } else {
      alert("check the url contain https:// or http://");
    }
  };

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="popup">
            <div className="popup_inner">
              <form onSubmit={this.onFormSubmit}>
                <div className="form-row">
                  <div className="col-md-2 mb-3" />
                  <div className="col-md-4 mb-3">
                    <label htmlFor="validationDefault01">Link</label>
                    <input
                      name="url_key"
                      type="text"
                      className="form-control"
                      id="validationDefault01"
                      placeholder="First name"
                      onChange={this.handleChange}
                      defaultValue={this.props.url.url_key}
                      required
                    />
                  </div>
                  <div className="col-md-4 mb-3">
                    <label htmlFor="validationDefault02"> Url</label>
                    <input
                      name="url"
                      type="text"
                      className="form-control"
                      id="validationDefault02"
                      placeholder="Last name"
                      onChange={this.handleChange}
                      defaultValue={this.props.url.url}
                      required
                    />
                  </div>
                </div>
                <div className="form-row">
                  <div className="col-md-2 mb-3" />
                  <div className="col-md-4 mb-3">
                    <button className="btn btn-primary" type="submit">
                      Submit form
                    </button>
                  </div>
                  <div className="col-md-4 mb-3">
                    <button
                      className="btn btn-primary"
                      type="button"
                      onClick={() => {
                        this.props.onBtnClick("delete");
                      }}
                    >
                      Cancel
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Edite;
