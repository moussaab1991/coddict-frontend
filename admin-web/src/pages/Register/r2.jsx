// import React, { Component } from "react";
// import { Link } from "react-router-dom";
// import Requests from "../../config/Requests";

// class Register extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       name: "",
//       email: "",
//       password: "",
//       password_confirmation: ""
//     };
//   }

//   onSubmit(e) {
//     e.preventDefault();
//     const { name, email, password, password_confirmation } = this.state;
//     console.log("registration", this.state);
//     Requests.post("signup", {
//       name,
//       email,
//       password,
//       password_confirmation
//     })
//       .then(response => {
//         this.setState({ err: false });
//       })
//       .catch(error => {
//         this.refs.name.value = "";
//         this.refs.password.value = "";
//         this.refs.email.value = "";
//         this.refs.confirm.value = "";
//         this.setState({ err: true });
//       });
//   }

//   onChange(e) {
//     const { name, value } = e.target;
//     this.setState({ [name]: value });
//   }

//   render() {
//     let error = this.state.err;
//     let msg = !error
//       ? "Registered Successfully"
//       : "Oops! , Something went wrong.";
//     let name = !error ? "alert alert-success" : "alert alert-danger";
//     return (
//       <div>
//         <div className="container">
//           <div className="row">
//             <div className="col-lg-8">
//               <div className="panel panel-default">
//                 <div className="panel-body">
//                   <div className="col-md-offset-2 col-md-8 col-md-offset-2">
//                     {error != undefined && (
//                       <div className={name} role="alert">
//                         {msg}
//                       </div>
//                     )}
//                   </div>
//                   <form
//                     role="form"
//                     method="POST"
//                     onSubmit={this.onSubmit.bind(this)}
//                   >
//                     <div className="form-row">
//                       <div className="col-6">
//                         <div className="form-group">
//                           <label htmlFor="name" className="control-label">
//                             Name
//                           </label>

//                           <div className="">
//                             <input
//                               id="name"
//                               type="text"
//                               className="form-control"
//                               ref="name"
//                               name="name"
//                               onChange={this.onChange.bind(this)}
//                               required
//                               autoFocus
//                             />
//                           </div>
//                         </div>
//                       </div>
//                       <div className="col-6">
//                         <div className="form-group">
//                           <label htmlFor="email" className="control-label">
//                             E-Mail Address
//                           </label>

//                           <div className="">
//                             <input
//                               id="email"
//                               type="email"
//                               className="form-control"
//                               ref="email"
//                               name="email"
//                               onChange={this.onChange.bind(this)}
//                               required
//                             />
//                           </div>
//                         </div>
//                       </div>
//                       <div className="col-6">
//                         <div className="form-group">
//                           <label htmlFor="password" className="control-label">
//                             Password
//                           </label>

//                           <div className="">
//                             <input
//                               id="password"
//                               type="password"
//                               className="form-control"
//                               ref="password"
//                               name="password"
//                               onChange={this.onChange.bind(this)}
//                               required
//                             />
//                           </div>
//                         </div>
//                       </div>
//                       <div className="col-6">
//                         <div className="form-group">
//                           <label
//                             htmlFor="password-confirm"
//                             className="control-label"
//                           >
//                             Confirm Password
//                           </label>

//                           <div className="">
//                             <input
//                               id="password-confirm"
//                               type="password"
//                               className="form-control"
//                               ref="confirm"
//                               name="password_confirmation"
//                               onChange={this.onChange.bind(this)}
//                               required
//                             />
//                           </div>
//                         </div>
//                       </div>

//                       <div className="form-group">
//                         <div className="col-md-6 col-md-offset-4">
//                           <button type="submit" className="btn btn-primary">
//                             Register
//                           </button>
//                         </div>
//                       </div>
//                     </div>
//                   </form>
//                 </div>
//               </div>
//             </div>
//           </div>
//         </div>
//       </div>
//     );
//   }
// }

// export default Register;
import React, { Component } from "react";
import OpenQuestion from "./component/OpenQuestion";
import MultipleChoise from "./component/MultipleChoise";

class AddQuestion extends Component {
  constructor(props) {
    super(props);
    this.state = {
      questions: []
    };
    this.inputRef = React.createRef();
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  addQuestion() {
    console.log(this.state.inputs);
  }
  handleChange(event) {
    console.log(event.target.value);
    const questionType = event.target.value;
    const copy_questions = [...this.state.questions];
    copy_questions.push({
      questionType: questionType,
      question: "",
      response: []
    });
    this.setState({ questions: copy_questions });
    console.log(this.state.questions);
    event.target.value = "select menu";
  }
  handleSubmit(e) {
    e.preventDefault();
    console.log(this.state.inputs);
  }

  deleteQuestion(i) {
    const copy_questions = [...this.state.questions];
    copy_questions.splice(i, 1);
    this.setState({ questions: copy_questions });
  }
  render() {
    return (
      <div className="form-center">
        <form onSubmit={this.handleSubmit}>
          <div className="form-row">
            <div className="form-group col-md-8">
              <label htmlFor="inputText">Name</label>
              <input
                type="text"
                className="form-control"
                id="inputText"
                placeholder="Assignment Name"
              />
            </div>
            <div className="form-group col-md-4">
              <label htmlFor="inputGrade">Grade</label>
              <input
                type="number"
                className="form-control"
                id="inputGrade"
                placeholder="Grade"
              />
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-md-6">
              <label htmlFor="inputImage">Image</label>
              <div className="custom-file">
                <input
                  type="file"
                  className="custom-file-input"
                  id="customImage"
                />
                <label className="custom-file-label" htmlFor="customImage">
                  Choose Image
                </label>
              </div>
            </div>
            <div className="form-group col-md-4">
              <label htmlFor="inputAction">Action</label>
              <select id="inputAction" className="form-control">
                <option value="flip-right" defaultValue>
                  flip-right
                </option>
                <option value="flip-left">flip-left</option>
                <option value="flip-up">flip-up</option>
                <option value="flip-down">flip-down</option>
                <option value="flip-diagonal-right">flip-diagonal-right</option>
                <option value="flip-diagonal-left">flip-diagonal-left</option>
                <option value="flip-inverted-diagonal-right">
                  flip-inverted-diagonal-right
                </option>
                <option value="flip-inverted-diagonal-left">
                  flip-inverted-diagonal-left
                </option>
              </select>
            </div>
          </div>
          <div>
            {this.state.questions.map((value, index) => {
              if (value.questionType == "OpenQuestion") {
                return (
                  <OpenQuestion
                    key={index}
                    index={index}
                    onDelete={this.deleteQuestion.bind(this)}
                  />
                );
              } else {
                return (
                  <MultipleChoise
                    key={index}
                    index={index}
                    onDelete={this.deleteQuestion.bind(this)}
                  />
                );
              }
            })}
          </div>
          <div className="input-group mb-3">
            <div className="input-group-prepend">
              <label className="input-group-text" htmlFor="inputGroupSelect01">
                Options
              </label>
            </div>
            <select
              className="custom-select"
              id="inputGroupSelect01"
              onChange={this.handleChange}
              ref={this.inputRef}
            >
              <option defaultValue>select menu</option>
              <option value="OpenQuestion">OpenQuestion</option>
              <option value="MultipleChoises">Multiple Choises</option>
              <option value="Image">Image</option>
            </select>
          </div>
          <button type="submit" className="btn btn-primary">
            Sign in
          </button>
        </form>{" "}
      </div>
    );
  }
