import React, { Component } from "react";
import { Link } from "react-router-dom";
import Requests from "../../config/Requests";

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      email: "",
      password: "",
      password_confirmation: "",
      type: "user"
    };
    this.onChange = this.onChange.bind(this);
  }

  onSubmit(e) {
    e.preventDefault();
    const { name, email, password, password_confirmation, type } = this.state;

    Requests.post("signup", {
      name,
      email,
      password,
      password_confirmation,
      type
    })
      .then(response => {
        console.log(response);
        this.setState({ err: false });
      })
      .catch(error => {
        this.refs.name.value = "";
        this.refs.password.value = "";
        this.refs.email.value = "";
        this.refs.confirm.value = "";
        //this.refs.type.value = "";
        this.setState({ err: true });
      });
  }

  onChange(e) {
    const { name, value } = e.target;
    console.log({ name, value });
    this.setState({ [name]: value });
    console.log(this.state);
  }

  render() {
    let error = this.state.err;
    let msg = !error
      ? "Registered Successfully"
      : "Oops! , Something went wrong.";
    let name = !error ? "alert alert-success" : "alert alert-danger";
    return (
      <div>
        <div className="container">
          <div className="row">
            <div className="col-lg-8">
              <div className="panel panel-default">
                <div className="panel-body">
                  <div className="col-md-offset-2 col-md-8 col-md-offset-2">
                    {error != undefined && (
                      <div className={name} role="alert">
                        {msg}
                      </div>
                    )}
                  </div>
                  <form
                    role="form"
                    method="POST"
                    onSubmit={this.onSubmit.bind(this)}
                  >
                    <div className="form-row">
                      <div className="col-6">
                        <div className="form-group">
                          <label htmlFor="name" className="control-label">
                            Name
                          </label>

                          <div className="">
                            <input
                              id="name"
                              type="text"
                              className="form-control"
                              ref="name"
                              name="name"
                              onChange={this.onChange.bind(this)}
                              required
                              autoFocus
                            />
                          </div>
                        </div>
                      </div>
                      <div className="col-6">
                        <div className="form-group">
                          <label htmlFor="email" className="control-label">
                            E-Mail Address
                          </label>

                          <div className="">
                            <input
                              id="email"
                              type="email"
                              className="form-control"
                              ref="email"
                              name="email"
                              onChange={this.onChange.bind(this)}
                              required
                            />
                          </div>
                        </div>
                      </div>
                      <div className="col-6">
                        <div className="form-group">
                          <label htmlFor="password" className="control-label">
                            Password
                          </label>

                          <div className="">
                            <input
                              id="password"
                              type="password"
                              className="form-control"
                              ref="password"
                              name="password"
                              onChange={this.onChange.bind(this)}
                              required
                            />
                          </div>
                        </div>
                      </div>
                      <div className="col-6">
                        <div className="form-group">
                          <label
                            htmlFor="password-confirm"
                            className="control-label"
                          >
                            Confirm Password
                          </label>

                          <div className="">
                            <input
                              id="password-confirm"
                              type="password"
                              className="form-control"
                              ref="confirm"
                              name="password_confirmation"
                              onChange={this.onChange.bind(this)}
                              required
                            />
                          </div>
                        </div>
                      </div>

                      <div className="col-6">
                        <div className="input-group mb-3">
                          <div className="input-group-prepend">
                            <label
                              className="input-group-text"
                              htmlFor="inputGroupSelect01"
                            >
                              Type
                            </label>
                          </div>
                          <select
                            className="custom-select"
                            id="inputGroupSelect01"
                            name="type"
                            onChange={this.onChange.bind(this)}
                          >
                            <option defaultValue value="admin">
                              Admin
                            </option>
                            <option value="user">User</option>
                          </select>
                        </div>
                      </div>
                      <div className="col-6">
                        <div className="form-group">
                          <div className="col-md-6 col-md-offset-4">
                            <button type="submit" className="btn btn-primary">
                              Register
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Register;
