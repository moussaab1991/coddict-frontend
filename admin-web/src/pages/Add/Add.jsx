import React, { Component } from "react";
import Requests from "../../config/Requests";
import { checkUrl } from "../../config/helper";
class Add extends Component {
  constructor(props) {
    super(props);
    this.state = {
      url_key: "",
      url: ""
    };
  }

  handleChange = e => {
    e.preventDefault();
    //Handling Multiple Inputs ;
    const target = e.target;
    const name = target.name;
    let value = target.value;

    this.setState({
      [name]: value
    });
    console.log(this.state);
  };

  onFormSubmit = e => {
    e.preventDefault();
    let urlWeb = checkUrl(this.state.url);
    if (urlWeb) {
      const formData = { url_key: this.state.url_key, url: this.state.url };

      Requests.post("/key", formData)
        .then(response => {
          console.log(response);
          this.setState({ err: false });
        })
        .catch(error => {
          this.refs.url_key.value = "";
          this.refs.url.value = "";
          this.setState({ err: true });
        });
    } else {
      alert("check the url contain https:// or http://");
    }
  };
  render() {
    let error = this.state.err;
    let msg = !error
      ? "Short Link has been added"
      : "Oops! , Something went wrong.";
    let name = !error ? "alert alert-success" : "alert alert-danger";

    return (
      <div>
        <div className="container">
          <div className="row">
            <div className="col-lg-8">
              <div className="panel panel-default">
                <div className="panel-body">
                  <div className="col-md-offset-2 col-md-8 col-md-offset-2">
                    {error != undefined && (
                      <div className={name} role="alert">
                        {msg}
                      </div>
                    )}
                  </div>
                  <form onSubmit={this.onFormSubmit}>
                    <div className="form-row">
                      <div className="col-6 mb-3">
                        <label htmlFor="validationDefault01" />
                        <input
                          name="url_key"
                          ref="url_key"
                          type="text"
                          className="form-control"
                          id="validationDefault01"
                          placeholder="Short Link"
                          onChange={this.handleChange}
                          required
                        />
                      </div>
                      <div className="col-6 mb-3">
                        <label htmlFor="validationDefault02" />
                        <input
                          name="url"
                          ref="url"
                          type="text"
                          className="form-control"
                          id="validationDefault02"
                          placeholder="Web Url"
                          onChange={this.handleChange}
                          required
                        />
                      </div>
                    </div>
                    <div className="form-row">
                      <div className="col-md-6 mb-3">
                        <button className="btn btn-primary" type="submit">
                          Submit form
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Add;
