import React, { Component } from "react";
import Requests from "../../config/Requests";
import Edite from "../Edite/Edite";
class All extends Component {
  constructor(props) {
    super(props);
    this.state = {
      urls: [],
      showPopup: { show: false, data: {} },
      urlPrev: "/url",
      urlNext: "/url"
    };
  }

  componentDidMount() {
    this.getData();
    // Requests.get("/url").then(response => {
    //   this.setState({ urls: response.data.data });
    // });
  }

  getData = btn => {
    let url = "/url";
    let preUrl = this.state.urlPrev;
    let nextUrl = this.state.urlNext;
    if (btn == "prev" && preUrl !== null) {
      url = preUrl;
    } else if (btn !== "prev" && nextUrl !== null) {
      url = nextUrl;
    }
    console.log("asdsad", url);
    Requests.get(url).then(response => {
      // console.log(response);
      this.setState({
        urls: response.data.data,
        urlPrev: response.data.prev_page_url,
        urlNext: response.data.next_page_url
      });
      // console.log(this.state);
    });
  };

  onDelete = id => {
    Requests.delete("/delete/" + id).then(response => {
      var urlsCopy = this.state.urls;
      for (let i = 0; i < urlsCopy.length; i++) {
        if (urlsCopy[i].id === id) {
          urlsCopy.splice(i, 1);
          this.setState({ urls: urlsCopy });
        }
      }
    });
  };
  editbtns = (btnType, data) => {
    if (btnType == "update") {
      this.onUpdate(data);
    } else {
      this.togglePopup();
    }
  };
  onUpdate = editForm => {
    Requests.post("/update/" + editForm.id, editForm).then(response => {
      console.log(response);
      var urlsCopy = this.state.urls;
      console.log("hello", urlsCopy);
      for (let i = 0; i < urlsCopy.length; i++) {
        if (urlsCopy[i].id === editForm.id) {
          urlsCopy[i] = editForm;
          this.setState({ urls: urlsCopy });
        }
      }
      this.togglePopup();
    });
  };

  togglePopup = url => {
    let showPopupCopy = { show: !this.state.showPopup.show, data: url };

    this.setState({
      showPopup: showPopupCopy
    });
  };

  show = () => {
    return this.state.showPopup.show ? (
      <Edite url={this.state.showPopup.data} onBtnClick={this.editbtns} />
    ) : null;
  };

  render() {
    return (
      <div className="table-responsive">
        <table className="table">
          <thead>
            <tr>
              {/* <th scope="col">No.</th> */}
              <th scope="col">Short Link</th>
              <th scope="col">Web Url</th>
              <th scope="col">Edit</th>
              <th scope="col">Delete</th>
            </tr>
          </thead>
          <tbody>
            {this.state.urls.map(url => {
              return (
                <tr key={url.id}>
                  {/* <th scope="row">{url.id}</th> */}
                  <td>{url.url_key}</td>
                  <td>{url.url}</td>
                  <td>
                    <a href="#" onClick={() => this.togglePopup(url)}>
                      Edit
                    </a>
                  </td>
                  <td>
                    <a href="#" onClick={() => this.onDelete(url.id)}>
                      Delete
                    </a>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <div className="card-footer">
          <button onClick={() => this.getData("prev")}>Back</button>
          <button onClick={() => this.getData("Next")}>Next</button>
        </div>
        {this.show()}
      </div>
    );
  }
}

export default All;
